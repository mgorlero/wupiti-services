package com.wupiti.service;

import com.wupiti.dao.CategoryDao;
import com.wupiti.dao.DiscountDao;
import com.wupiti.dao.PostDao;
import com.wupiti.dao.SearchDao;
import com.wupiti.dao.UserDao;
import com.wupiti.domain.Categories;
import com.wupiti.domain.Category;
import com.wupiti.domain.discount.Discount;
import com.wupiti.domain.discount.Discounts;
import com.wupiti.domain.post.Post;
import com.wupiti.domain.post.Posts;
import com.wupiti.domain.search.SearchDiscounts;
import com.wupiti.domain.user.User;

public class ServiceFacade {

	private SearchDao searchDao;
	private DiscountDao discountDao;
	private PostDao postDao;
	private UserDao userDao;
	private CategoryDao categoryDao;

	public SearchDiscounts find(String name) {
		return searchDao.find(name);
	}
	
	public SearchDiscounts getAll() {
		return searchDao.getAll();
	}
	
	public SearchDiscounts findNear(Double lat, Double lon, Double distance) {
		return searchDao.findNear(lat, lon, distance);
	}

	public SearchDiscounts findNear(Double lat1, Double lon1, Double lat2, Double lon2) {
		return searchDao.findNear(lat1, lon1, lat2, lon2);
	}
	
	public SearchDiscounts findNear(Double lat, Double lon, Integer category) {
		return searchDao.findNear(lat, lon, category);
	}
	
	public SearchDiscounts getDiscountsByCategory(Integer category){
		return searchDao.getDiscountsByCategory(category);
	}
	
	/* ---------------- NEW SERVICE v2 --------------- */
	
	public SearchDiscounts getDiscountsByCategoryv2(Integer category){
		return searchDao.getDiscountsByCategoryv2(category);
	}
	
	public SearchDiscounts findNearv2(Double lat1, Double lon1, Double lat2, Double lon2) {
		return searchDao.findNear(lat1, lon1, lat2, lon2);
	}
	
	public SearchDiscounts getAllv2() {
		return searchDao.getAllv2();
	}
	
	public Post createPost(Post post) {
		return postDao.create(post);
	}
	
	public Posts getUserPosts(String userId){
		
		return postDao.getUserPosts(userId);
	}
	
	
	
	public User createUser(User user) {
		return userDao.create(user);
	}

	public Discount getDiscount(String discountId) {
		return discountDao.getDiscount(discountId);
	}
	
	public Discounts getDiscountsAll() {
		return discountDao.getDiscountsAll();
	}
	
	public Discounts getDiscountsAllv2() {
		return discountDao.getDiscountsAllv2();
	}
	
	public Category getCategory(String categoryId) {
		return categoryDao.getCategory(categoryId);
	}
	
	public Categories getCategoriesAll() {
		return categoryDao.getCategoriesAll();
	}
	
	public void setSearchDao(SearchDao searchDao) {
		this.searchDao = searchDao;
	}

	public void setDiscountDao(DiscountDao discountDao) {
		this.discountDao = discountDao;
	}
	
	public void setCategoryDao(CategoryDao categoryDao) {
		this.categoryDao = categoryDao;
	}

	public void setPostDao(PostDao postDao) {
		this.postDao = postDao;
	}
	
	public void setUserDao(UserDao userDao) {
		this.userDao = userDao;
	}
	
}
