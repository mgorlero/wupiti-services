package com.wupiti.domain;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "place")
public class Place {

	private String _id;
	private String name;
	private String ownerDescription;
	private Manager manager;
	private Branches[] branches;
	
	public Place() {

	}

	public String get_id() {
		return _id;
	}

	public void set_id(String _id) {
		this._id = _id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getOwnerDescription() {
		return ownerDescription;
	}

	public void setOwnerDescription(String ownerDescription) {
		this.ownerDescription = ownerDescription;
	}

	public Manager getManager() {
		return manager;
	}

	public void setManager(Manager manager) {
		this.manager = manager;
	}

	public Branches[] getBranches() {
		return branches;
	}

	public void setBranches(Branches[] branches) {
		this.branches = branches;
	}
	
	
	
	
}
