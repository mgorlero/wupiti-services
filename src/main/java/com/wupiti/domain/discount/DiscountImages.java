package com.wupiti.domain.discount;

public class DiscountImages {

	private String[] image;

	public String[] getImage() {
		return image;
	}

	public void setImage(String[] image) {
		this.image = image;
	}
	
}
