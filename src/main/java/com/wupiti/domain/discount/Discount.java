package com.wupiti.domain.discount;

import java.util.Date;

import javax.xml.bind.annotation.XmlRootElement;

import org.springframework.data.mongodb.core.mapping.Document;

import com.wupiti.domain.search.SearchPlace;

@XmlRootElement(name = "discounts")
@Document(collection = "discounts")
public class Discount implements Comparable<Discount>{

	private String _id;
	private String shortDescription;
	private String description;
	private Date expiration;
	private Date created;
	private String discountMessage;
	private Conditions conditions;
	private SearchPlace place;
	private DiscountImages images;
	private String status;
	private String stockImage;
	private Boolean checkAdult;
	private Boolean hideOldApp;
	
	public String get_id() {
		return _id;
	}
	public void set_id(String _id) {
		this._id = _id;
	}
	public String getShortDescription() {
		return shortDescription;
	}
	public void setShortDescription(String shortDescription) {
		this.shortDescription = shortDescription;
	}
	public Date getExpiration() {
		return expiration;
	}
	public void setExpiration(Date expiration) {
		this.expiration = expiration;
	}
	public String getDiscountMessage() {
		return discountMessage;
	}
	public void setDiscountMessage(String discountMessage) {
		this.discountMessage = discountMessage;
	}
	public Conditions getConditions() {
		return conditions;
	}
	public void setConditions(Conditions conditions) {
		this.conditions = conditions;
	}
	public SearchPlace getPlace() {
		return place;
	}
	public void setPlace(SearchPlace place) {
		this.place = place;
	}
	public DiscountImages getImages() {
		return images;
	}
	public void setImages(DiscountImages images) {
		this.images = images;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Date getCreated() {
		return created;
	}
	public void setCreated(Date created) {
		this.created = created;
	}
	
	@Override
	public int hashCode() {
		return _id.hashCode();
	}
	@Override
	public boolean equals(Object obj) {
		return _id.equals(((Discount)obj).get_id());
	}

	@Override
	public int compareTo(Discount o) {
		return _id.compareTo(o.get_id());
	}
	public String getStockImage() {
		return stockImage;
	}
	public void setStockImage(String stockImage) {
		this.stockImage = stockImage;
	}
	public Boolean getCheckAdult() {
		return checkAdult;
	}
	public void setCheckAdult(Boolean checkAdult) {
		this.checkAdult = checkAdult;
	}
	public Boolean getHideOldApp() {
		return hideOldApp;
	}
	public void setHideOldApp(Boolean hideOldApp) {
		this.hideOldApp = hideOldApp;
	}
	
}
