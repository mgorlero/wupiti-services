package com.wupiti.domain.post;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import org.bson.types.ObjectId;
import com.wupiti.domain.adapter.ObjectIdAdapter;

@XmlRootElement(name = "post")
public class Post implements Comparable<Post> {

	private String _id;
	private String networkPostId;
	private String postNetwork;
	private String userId;
	private ObjectId discountId;
	private Integer placeId;
	private String branchId;
	private String image;
	private String postMessage;
	private String cuponBarcode;
	
	private String discountDesc;
	private String placeName;
	private String date;
	
	private Boolean isStockImagePost;

	@XmlElement(name="_id")
	public String get_id() {
		return _id;
	}

	public void set_id(String _id) {
		this._id = _id;
	}

	public String getNetworkPostId() {
		return networkPostId;
	}

	public void setNetworkPostId(String networkPostId) {
		this.networkPostId = networkPostId;
	}

	public String getPostNetwork() {
		return postNetwork;
	}

	public void setPostNetwork(String postNetwork) {
		this.postNetwork = postNetwork;
	}
	
	@XmlJavaTypeAdapter(ObjectIdAdapter.class)
	@XmlElement(name="discountId")
	public ObjectId getDiscountId() {
		return discountId;
	}

	public void setDiscountId(ObjectId discountId) {
		this.discountId = discountId;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getCuponBarcode() {
		if (cuponBarcode != null) {
			//MessageDigestPasswordEncoder encoder = new MessageDigestPasswordEncoder(
				//	"SHA-1");
			//cuponBarcode = encoder.encodePassword(postNetwork + networkPostId
			//		+ userId.toString(), "salt goes here");
			
			cuponBarcode = postNetwork + networkPostId + userId.toString();
		}
		return cuponBarcode;
	}

	public void setCuponBarcode(String cuponBarcode) {
		//MessageDigestPasswordEncoder encoder = new MessageDigestPasswordEncoder(
			//	"SHA-1");
		//cuponBarcode = encoder.encodePassword(postNetwork + networkPostId
		//		+ userId.toString(), "salt goes here");
		//cuponBarcode = postNetwork + networkPostId;
		this.cuponBarcode = postNetwork + networkPostId + userId.toString();
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public Integer getPlaceId() {
		return placeId;
	}

	public void setPlaceId(Integer placeId) {
		this.placeId = placeId;
	}

	public String getBranchId() {
		return branchId;
	}

	public void setBranchId(String branchId) {
		this.branchId = branchId;
	}
	
	@Override
	public int hashCode() {
		return _id.hashCode();
	}
	@Override
	public boolean equals(Object obj) {
		return _id.equals(((Post)obj).get_id());
	}

	@Override
	public int compareTo(Post o) {
		return _id.compareTo(o.get_id());
	}

	public String getDiscountDesc() {
		return discountDesc;
	}

	public void setDiscountDesc(String discountDesc) {
		this.discountDesc = discountDesc;
	}

	public String getPlaceName() {
		return placeName;
	}

	public void setPlaceName(String placeName) {
		this.placeName = placeName;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getPostMessage() {
		return postMessage;
	}

	public void setPostMessage(String postMessage) {
		this.postMessage = postMessage;
	}

	public Boolean getIsStockImagePost() {
		return isStockImagePost;
	}

	public void setIsStockImagePost(Boolean isStockImagePost) {
		this.isStockImagePost = isStockImagePost;
	}

}
