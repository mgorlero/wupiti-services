package com.wupiti.domain.post;

import java.util.Set;
import java.util.TreeSet;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "posts")
public class Posts {

	private Set<Post> post = new TreeSet<Post>();

	public Set<Post> getPost() {
		return post;
	}

	public void setPost(Set<Post> post) {
		this.post = post;
	}

}
