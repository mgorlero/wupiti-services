package com.wupiti.domain;

import java.util.Set;
import java.util.TreeSet;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "categories")
public class Categories {
	
	private Set<Category> category = new TreeSet<Category>();

	public Set<Category> getCategory() {
		return category;
	}

	public void setCategory(Set<Category> category) {
		this.category = category;
	}

}
