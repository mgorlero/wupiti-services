package com.wupiti.domain;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "geo")
public class Geo {

	private Long lon;
	private Long lat;
	public Long getLon() {
		return lon;
	}
	public void setLon(Long lon) {
		this.lon = lon;
	}
	public Long getLat() {
		return lat;
	}
	public void setLat(Long lat) {
		this.lat = lat;
	}
	
	
}
