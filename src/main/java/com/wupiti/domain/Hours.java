package com.wupiti.domain;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "hours")
public class Hours {

	private String day;
	private String open;
	private String close;
	public String getClose() {
		return close;
	}
	public void setClose(String close) {
		this.close = close;
	}
	public String getOpen() {
		return open;
	}
	public void setOpen(String open) {
		this.open = open;
	}
	public String getDay() {
		return day;
	}
	public void setDay(String day) {
		this.day = day;
	}
	
	
	
	
}
