package com.wupiti.domain;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "branches")
public class Branches {
	
	private String id;
	private String details;
	private String phoneNumber;
	private String email;
	
	private Geo geo;
	private Address address;
	private Hours hours;
	
	public String getDetails() {
		return details;
	}
	public void setDetails(String details) {
		this.details = details;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public Geo getGeo() {
		return geo;
	}
	public void setGeo(Geo geo) {
		this.geo = geo;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public Address getAddress() {
		return address;
	}
	public void setAddress(Address address) {
		this.address = address;
	}
	public Hours getHours() {
		return hours;
	}
	public void setHours(Hours hours) {
		this.hours = hours;
	}
	
	
//	"geo" : {
//	"lon" : 1,
//	"lat" : 1
//},
//"details" : "algo",
//"phoneNumber" : "23422",
//"email" : "wupiti@wupiti.com"
}
