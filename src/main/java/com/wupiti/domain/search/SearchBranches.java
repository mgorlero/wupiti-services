package com.wupiti.domain.search;

import java.util.List;

public class SearchBranches {

	private List<SearchBranch> branch;

	public List<SearchBranch> getBranch() {
		return branch;
	}

	public void setBranch(List<SearchBranch> branch) {
		this.branch = branch;
	}
	
	
}
