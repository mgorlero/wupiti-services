package com.wupiti.domain.search;

import java.util.List;


public class SearchCategories {

	private List<SearchCategory> category;

	public List<SearchCategory> getCategory() {
		return category;
	}

	public void setCategory(List<SearchCategory> category) {
		this.category = category;
	}

}
