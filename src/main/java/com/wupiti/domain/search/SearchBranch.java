package com.wupiti.domain.search;


public class SearchBranch {
	private Integer branchId;
	private Double lat;
	private Double lon;
	private String details;
	private String description;
	private String phone;
	
	private String facebookPlace;
	
	public Integer getBranchId() {
		return branchId;
	}
	public void setBranchId(Integer branchId) {
		this.branchId = branchId;
	}
	public Double getLat() {
		return lat;
	}
	public void setLat(Double lat) {
		this.lat = lat;
	}
	public Double getLon() {
		return lon;
	}
	public void setLon(Double lon) {
		this.lon = lon;
	}
	public String getDetails() {
		return details;
	}
	public void setDetails(String details) {
		this.details = details;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getFacebookPlace() {
		return facebookPlace;
	}
	public void setFacebookPlace(String facebookPlace) {
		this.facebookPlace = facebookPlace;
	}


	
}
