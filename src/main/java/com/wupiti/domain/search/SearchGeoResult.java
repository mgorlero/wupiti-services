package com.wupiti.domain.search;

import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "geoindex")
public class SearchGeoResult {

	private Long placeId;
	private Long branchId;
	public Long getPlaceId() {
		return placeId;
	}
	public void setPlaceId(Long placeId) {
		this.placeId = placeId;
	}
	public Long getBranchId() {
		return branchId;
	}
	public void setBranchId(Long branchId) {
		this.branchId = branchId;
	}
	
}
