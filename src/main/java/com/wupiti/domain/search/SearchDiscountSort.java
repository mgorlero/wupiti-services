package com.wupiti.domain.search;

import java.util.Comparator;

public class SearchDiscountSort  implements Comparator<SearchDiscount> {
	  @Override
	  public int compare(SearchDiscount x, SearchDiscount y) {
	    // TODO: Handle null x or y values
	    int startComparison = compare(x.getPriority(), y.getPriority());
	    return startComparison != 0 ? startComparison
	                                : compare(x.getPriority(), y.getPriority());
	  }

	  // I don't know why this isn't in Long...
	  private static int compare(long a, long b) {
	    return a < b ? 1
	         : a > b ? -1
	         : 0;
	  }
	}