package com.wupiti.domain.search;

import java.util.Date;

import javax.xml.bind.annotation.XmlRootElement;

import org.springframework.data.mongodb.core.mapping.Document;

@XmlRootElement(name = "discount")
@Document(collection = "discounts")
public class SearchDiscount implements Comparable<SearchDiscount>{

	private String _id;
	private String shortDescription;
	private SearchPlace place;
	private String status;
	private Date created;
	private int priority;
	private Boolean hideOldApp;
	
	public String get_id() {
		return _id;
	}
	public void set_id(String _id) {
		this._id = _id;
	}
	public String getShortDescription() {
		return shortDescription;
	}
	public void setShortDescription(String shortDescription) {
		this.shortDescription = shortDescription;
	}
	public SearchPlace getPlace() {
		return place;
	}
	public void setPlace(SearchPlace place) {
		this.place = place;
	}
	@Override
	public int hashCode() {
		return _id.hashCode();
	}
	@Override
	public boolean equals(Object obj) {
		return _id.equals(((SearchDiscount)obj).get_id());
	}

	@Override
	public int compareTo(SearchDiscount o) {
		return _id.compareTo(o.get_id());
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Date getCreated() {
		return created;
	}
	public void setCreated(Date created) {
		this.created = created;
	}
	public int getPriority() {
		return priority;
	}
	public void setPriority(int priority) {
		this.priority = priority;
	}
	public Boolean getHideOldApp() {
		return hideOldApp;
	}
	public void setHideOldApp(Boolean hideOldApp) {
		this.hideOldApp = hideOldApp;
	}
	
}
