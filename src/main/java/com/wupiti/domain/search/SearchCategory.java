package com.wupiti.domain.search;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "category")
public class SearchCategory {

	private Integer categoryId;
	private String name;
	private Integer parent;
	
	
	public Integer getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(Integer categoryId) {
		this.categoryId = categoryId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Integer getParent() {
		return parent;
	}
	public void setParent(Integer parent) {
		this.parent = parent;
	}
	
	
}
