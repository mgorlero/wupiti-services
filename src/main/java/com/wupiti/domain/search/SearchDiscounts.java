package com.wupiti.domain.search;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "discounts")
public class SearchDiscounts {

	//private Set<SearchDiscount> discount = new TreeSet<SearchDiscount>();

	private List<SearchDiscount> discount = new ArrayList<SearchDiscount>();
	
	public List<SearchDiscount> getDiscount() {
		return discount;
	}

	public void setDiscount(List<SearchDiscount> discounts) {
		this.discount = discounts;
	}
	
}
