package com.wupiti.domain.search;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "place")
public class SearchPlace {

	private Integer placeId;
	private String name;
	
	private SearchBranches branches;
	private SearchCategories categories;
	
	public Integer getPlaceId() {
		return placeId;
	}
	public void setPlaceId(Integer placeId) {
		this.placeId = placeId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public SearchBranches getBranches() {
		return branches;
	}
	public void setBranches(SearchBranches branches) {
		this.branches = branches;
	}
	public SearchCategories getCategories() {
		return categories;
	}
	public void setCategories(SearchCategories categories) {
		this.categories = categories;
	}
	
	
}
