package com.wupiti.domain.user;

import java.util.Date;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import com.wupiti.domain.adapter.DateAdapter;


public class FacebookCredentials {
	
	private String code;
	private String issuedAt;
	private String userId;
	private String oauthToken;
	private String expires;
	private String username;
	private String friendsCount;
	
	//private FacebookUser user;
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	//@XmlJavaTypeAdapter(DateAdapter.class)
	//@XmlElement(name="issuedAt")
	public String getIssuedAt() {
		return issuedAt;
	}
	public void setIssuedAt(String issuedAt) {
		this.issuedAt = issuedAt;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getOauthToken() {
		return oauthToken;
	}
	public void setOauthToken(String oauthToken) {
		this.oauthToken = oauthToken;
	}
	
	//@XmlJavaTypeAdapter(DateAdapter.class)
	//@XmlElement(name="expires")
	public String getExpires() {
		return expires;
	}
	public void setExpires(String expires) {
		this.expires = expires;
	}
	/*public FacebookUser getUser() {
		return user;
	}
	public void setUser(FacebookUser user) {
		this.user = user;
	}*/
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getFriendsCount() {
		return friendsCount;
	}
	public void setFriendsCount(String friendsCount) {
		this.friendsCount = friendsCount;
	}

}
