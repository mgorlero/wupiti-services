package com.wupiti.domain;

import javax.xml.bind.annotation.XmlRootElement;

import org.springframework.data.mongodb.core.mapping.Document;


@XmlRootElement(name = "category")
@Document(collection = "categories")
public class Category implements Comparable<Category>{
	
	private String _id;
	private String name;
	private Integer categoryId;
	private Integer parent;
	
	public String get_id() {
		return _id;
	}
	public void set_id(String _id) {
		this._id = _id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Integer getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(Integer categoryId) {
		this.categoryId = categoryId;
	}
	public Integer getParent() {
		return parent;
	}
	public void setParent(Integer parent) {
		this.parent = parent;
	}

	@Override
	public int hashCode() {
		return _id.hashCode();
	}
	@Override
	public boolean equals(Object obj) {
		return _id.equals(((Category)obj).get_id());
	}

	@Override
	public int compareTo(Category o) {
		return _id.compareTo(o.get_id());
	}

	

}
