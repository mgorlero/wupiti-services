package com.wupiti.dao;

import com.wupiti.domain.post.Post;
import com.wupiti.domain.post.Posts;

public interface PostDao {

	public Post create(Post post);
	
	public Posts getUserPosts(String userId);
	
}
