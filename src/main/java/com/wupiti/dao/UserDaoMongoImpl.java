package com.wupiti.dao;

import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import com.wupiti.domain.user.User;

public class UserDaoMongoImpl implements UserDao {

	MongoOperations mongoOperation;
	
	@Override
	public User create(User user) {
		
		//find if user already exists
		User findUser = mongoOperation.findOne(
				new Query(Criteria
						.where("facebook.userId")
						.is(user.getFacebook().getUserId())), User.class);
	
		if(findUser != null){
			//User exists update data
			findUser.setBirthday(user.getBirthday());
			findUser.setEmail(user.getEmail());
			findUser.setLastName(user.getLastName());
			findUser.setName(user.getName());
			findUser.setLocation(user.getLocation());
			findUser.getFacebook().setFriendsCount(user.getFacebook().getFriendsCount());
			findUser.getFacebook().setExpires(user.getFacebook().getExpires());
			findUser.getFacebook().setIssuedAt(user.getFacebook().getIssuedAt());
			findUser.getFacebook().setOauthToken(user.getFacebook().getOauthToken());
			findUser.getFacebook().setUsername(user.getFacebook().getUsername());
			findUser.setDevice(user.getDevice());
		
			mongoOperation.save(findUser, "users");
			
		}else{	
			//New User
			mongoOperation.insert(user, "users");	
			findUser = mongoOperation.findOne(
					new Query(Criteria
							.where("facebook.userId")
							.is(user.getFacebook().getUserId())), User.class);
		}
		
		//findUser.setDevice(user.getCreated().toString());
		
		return findUser;
	}

	
	public void setMongoOperation(MongoOperations mongoOperation) {
		this.mongoOperation = mongoOperation;
	}
}
