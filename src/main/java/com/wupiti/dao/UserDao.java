package com.wupiti.dao;

import com.wupiti.domain.user.User;

public interface UserDao {

	public User create(User user); 
}
