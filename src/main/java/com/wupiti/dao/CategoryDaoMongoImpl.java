package com.wupiti.dao;

import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Query;

import com.wupiti.domain.Categories;
import com.wupiti.domain.Category;

public class CategoryDaoMongoImpl implements CategoryDao {

	MongoOperations mongoOperation;
	
	@Override
	public List<Category> findCategoryByPlace(Integer placeId) {
		return null;
	}

	@Override
	public Category getCategory(String categoryId) {
		Category category = mongoOperation.findById(new ObjectId(categoryId), Category.class);
		return category;
	}

	@Override
	public List<Category> findCategoryByPlaceBranch(Integer placeId,
			Integer branchId) {
		return null;
	}

	public void setMongoOperation(MongoOperations mongoOperation) {
		this.mongoOperation = mongoOperation;
	}

	@Override
	public Categories getCategoriesAll() {
		Query query = new Query();
		List<Category> categoriesFound = mongoOperation.find(query,
				Category.class, "categories");
		Categories categories = new Categories();
		categories.getCategory().addAll(categoriesFound);
		
		return categories;
	}
	

	

	
}
