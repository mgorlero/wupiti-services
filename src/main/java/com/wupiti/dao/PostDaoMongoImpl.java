package com.wupiti.dao;

import java.util.List;

import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import com.wupiti.domain.post.Post;
import com.wupiti.domain.post.Posts;


public class PostDaoMongoImpl implements PostDao {

	MongoOperations mongoOperation;

	@Override
	public Post create(Post post) {
		mongoOperation.insert(post, "posts");
		post = mongoOperation.findOne(
				new Query(Criteria
						.where("postNetwork")
						.is(post.getPostNetwork())
						.andOperator(
								Criteria.where("networkPostId").is(
										post.getNetworkPostId()))), Post.class);
		return post;
	}

	public void setMongoOperation(MongoOperations mongoOperation) {
		this.mongoOperation = mongoOperation;
	}

	@Override
	public Posts getUserPosts(String userId) {
		Posts posts = new Posts();
		Query query = new Query(Criteria.where("userId").is(userId));
		query.with(new Sort(Sort.Direction.DESC, "placeId"));
		List<Post> postsFound = mongoOperation.find(query,
					Post.class, "posts");
		posts.getPost().addAll(postsFound);
		
		return posts;
	}

}
