package com.wupiti.dao;

import java.util.Collections;
import java.util.List;

import org.springframework.data.domain.Sort;
import org.springframework.data.geo.Box;
import org.springframework.data.geo.GeoResult;
import org.springframework.data.geo.GeoResults;
import org.springframework.data.geo.Metrics;
import org.springframework.data.geo.Point;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.NearQuery;
import org.springframework.data.mongodb.core.query.Query;

import com.wupiti.domain.search.SearchDiscount;
import com.wupiti.domain.search.SearchDiscountSort;
import com.wupiti.domain.search.SearchDiscounts;
import com.wupiti.domain.search.SearchGeoResult;

public class SearchDaoMongoImpl implements SearchDao {

	MongoOperations mongoOperation;

	@Override
	public SearchDiscounts find(String text) {
		
		SearchDiscounts discounts = new SearchDiscounts();
		
		Query query = new Query();
		query.addCriteria(Criteria.where("place.name")
				.regex("^" + text + "", "i")
				.and("status").is("active"));
		List<SearchDiscount> discountsFound = mongoOperation.find(query,
				SearchDiscount.class, "discounts");
		discounts.getDiscount().addAll(discountsFound);
		
		return discounts;
	}
	
	@Override
	public SearchDiscounts findNear(Double latitud, Double longitud,
			Double distance) {

		NearQuery nearQuery = NearQuery.near(longitud, latitud, Metrics.KILOMETERS).maxDistance(distance);
		
		/*if(category != null) {
			Query query = new Query(Criteria.where("category").is(category));
			nearQuery.query(query);
		}*/

		GeoResults<SearchGeoResult> geos = mongoOperation.geoNear(nearQuery,
				SearchGeoResult.class, "geoindex");

		return getByGeoResult(geos);
	}
	
	@Override
	public SearchDiscounts findNear(Double lat1, Double lon1, Double lat2, Double lon2) {

		 Box box = new Box(new Point(lon1, lat1), new Point(lon2, lat2));
		 Query query = Query.query(Criteria.where("loc").within(box));
		 //nearQuery.query(query);
		 //.withinSphere(new Circle(p, new Distance(distance, Metrics.KILOMETERS).getNormalizedValue())));

		List<SearchGeoResult> geos = mongoOperation.find(query,
				SearchGeoResult.class, "geoindex");

		return getByWithinResult(geos);
	}
	
	@Override
	public SearchDiscounts findNearv2(Double lat1, Double lon1, Double lat2, Double lon2) {

		 Box box = new Box(new Point(lon1, lat1), new Point(lon2, lat2));
		 Query query = Query.query(Criteria.where("loc").within(box));
	
		List<SearchGeoResult> geos = mongoOperation.find(query,
				SearchGeoResult.class, "geoindex");

		return getByWithinResultv2(geos);
	}
	
	private SearchDiscounts getByWithinResultv2(List<SearchGeoResult> geos) {
		SearchDiscounts discounts = new SearchDiscounts();
		for (SearchGeoResult geoResult : geos) {
			Query query = new Query();
			
			
			query.addCriteria(Criteria.where("place.placeId").is(geoResult.getPlaceId())
					.and("status").is("active")
					.and("place.branches.branchId").is(geoResult.getBranchId()));
			
			List<SearchDiscount> discountsFound = mongoOperation.find(query,
					SearchDiscount.class, "discounts");
			discounts.getDiscount().addAll(discountsFound);
		}
		
		
		Collections.sort(discounts.getDiscount(), new SearchDiscountSort());	
		
		return discounts;
	}

	@Override
	public SearchDiscounts findNear(Double latitud, Double longitud,
			Integer category) {
		
		NearQuery nearQuery = NearQuery.near(longitud, latitud).inKilometers();
		if(category != null) {
			Query query = new Query(Criteria.where("category").is(category));
			nearQuery.query(query);
		}
		
		GeoResults<SearchGeoResult> geos = mongoOperation.geoNear(nearQuery, SearchGeoResult.class,
				"geoindex");

		return getByGeoResult(geos);
	}

	private SearchDiscounts getByGeoResult(GeoResults<SearchGeoResult> geos) {
		SearchDiscounts discounts = new SearchDiscounts();
		for (GeoResult<SearchGeoResult> geoResult : geos) {
			Query query = new Query();
			query.addCriteria(Criteria.where("place.placeId")
					.is(geoResult.getContent().getPlaceId())
					.and("status").is("active")
					.and("place.branches.branchId")
					.is(geoResult.getContent().getBranchId()));
			query.with(new Sort(Sort.Direction.DESC, "priority").and(new Sort(Sort.Direction.DESC, "created")));
			
			List<SearchDiscount> discountsFound = mongoOperation.find(query,
					SearchDiscount.class, "discounts");
			
		
			discounts.getDiscount().addAll(discountsFound);
		}
		return discounts;
	}
	
	private SearchDiscounts getByWithinResult(List<SearchGeoResult> geos) {
		SearchDiscounts discounts = new SearchDiscounts();
		for (SearchGeoResult geoResult : geos) {
			Query query = new Query();
			
			
			query.addCriteria(Criteria.where("place.placeId").is(geoResult.getPlaceId())
					.and("status").is("active")
					.and("hideOldApp").is(false)
					.and("place.branches.branchId").is(geoResult.getBranchId()));
			
			List<SearchDiscount> discountsFound = mongoOperation.find(query,
					SearchDiscount.class, "discounts");
			discounts.getDiscount().addAll(discountsFound);
		}
		
		
		Collections.sort(discounts.getDiscount(), new SearchDiscountSort());	
		
		return discounts;
	}

	public void setMongoOperation(MongoOperations mongoOperation) {
		this.mongoOperation = mongoOperation;
	}

	@Override
	public SearchDiscounts getAll() {
		SearchDiscounts discounts = new SearchDiscounts();
		Query query = new Query();
		
		query.addCriteria(Criteria.where("status").is("active").and("hideOldApp").is(false));
		query.with(new Sort(Sort.Direction.DESC, "priority").and(new Sort(Sort.Direction.DESC, "created")));

		List<SearchDiscount> discountsFound = mongoOperation.find(query,
				SearchDiscount.class, "discounts");
		discounts.getDiscount().addAll(discountsFound);
		
		return discounts;
	}
	
	@Override
	public SearchDiscounts getAllv2() {
		SearchDiscounts discounts = new SearchDiscounts();
		Query query = new Query();
		
		query.addCriteria(Criteria.where("status").is("active"));
		query.with(new Sort(Sort.Direction.DESC, "priority").and(new Sort(Sort.Direction.DESC, "created")));

		List<SearchDiscount> discountsFound = mongoOperation.find(query,
				SearchDiscount.class, "discounts");
		discounts.getDiscount().addAll(discountsFound);
		
		return discounts;
	}


	@Override
	public SearchDiscounts getDiscountsByCategory(Integer category) {
		SearchDiscounts discounts = new SearchDiscounts();
		
		Query query = new Query(Criteria.where("place.categories.parent").is(category)
				.and("status").is("active").and("hideOldApp").is(false));
		query.with(new Sort(Sort.Direction.DESC, "priority").and(new Sort(Sort.Direction.DESC, "created")));
		
		
		List<SearchDiscount> discountsFound = mongoOperation.find(query,
					SearchDiscount.class, "discounts");
		discounts.getDiscount().addAll(discountsFound);
		
		return discounts;
	}
	
	@Override
	public SearchDiscounts getDiscountsByCategoryv2(Integer category) {
		SearchDiscounts discounts = new SearchDiscounts();
		
		Query query = new Query(Criteria.where("place.categories.parent").is(category)
				.and("status").is("active"));
		query.with(new Sort(Sort.Direction.DESC, "priority").and(new Sort(Sort.Direction.DESC, "created")));
		
		
		List<SearchDiscount> discountsFound = mongoOperation.find(query,
					SearchDiscount.class, "discounts");
		discounts.getDiscount().addAll(discountsFound);
		
		return discounts;
	}
}
