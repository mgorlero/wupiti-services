package com.wupiti.dao;

import java.util.List;

import com.wupiti.domain.discount.Discount;
import com.wupiti.domain.discount.Discounts;

public interface DiscountDao {

	public List<Discount> findDiscountByPlace(Integer placeId, Integer branchId);
	
	public Discount getDiscount(String discountId);
	
	public Discounts getDiscountsAll();
	
	public Discounts getDiscountsAllv2();
	
	public List<Discount> findDiscountByPlaceBranch(Integer placeId, Integer branchId);
	
}
