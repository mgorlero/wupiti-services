package com.wupiti.dao;

import java.util.List;

import com.wupiti.domain.Categories;
import com.wupiti.domain.Category;

public interface CategoryDao {

	public List<Category> findCategoryByPlace(Integer placeId);
	
	public Category getCategory(String categoryId);
	
	public Categories getCategoriesAll();
	
	public List<Category> findCategoryByPlaceBranch(Integer placeId, Integer branchId);
}
