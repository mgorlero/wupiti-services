package com.wupiti.dao;

import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import com.wupiti.domain.discount.Discount;
import com.wupiti.domain.discount.Discounts;


public class DiscountDaoMongoImpl implements DiscountDao {

	MongoOperations mongoOperation;
	
	@Override
	public List<Discount> findDiscountByPlace(Integer placeId, Integer branchId) {
		return null;
	}

	@Override
	public Discount getDiscount(String discountId) {
		Discount discount = mongoOperation.findById(new ObjectId(discountId), Discount.class);
		return discount;
	}

	@Override
	public List<Discount> findDiscountByPlaceBranch(Integer placeId,
			Integer branchId) {
		return null;
	}

	public void setMongoOperation(MongoOperations mongoOperation) {
		this.mongoOperation = mongoOperation;
	}

	@Override
	public Discounts getDiscountsAll() {
		Query query = new Query();
		
		query.addCriteria(Criteria.where("status").is("active").and("hideOldApp").is(false));
		query.with(new Sort(Sort.Direction.DESC, "priority").and(new Sort(Sort.Direction.DESC, "created")));

		List<Discount> discountsFound = mongoOperation.find(query,
				Discount.class, "discounts");
		Discounts discounts = new Discounts();
		discounts.getDiscount().addAll(discountsFound);
		
		return discounts;
	}
	
	@Override
	public Discounts getDiscountsAllv2() {
		Query query = new Query();
		
		query.addCriteria(Criteria.where("status").is("active"));
		query.with(new Sort(Sort.Direction.DESC, "priority").and(new Sort(Sort.Direction.DESC, "created")));

		List<Discount> discountsFound = mongoOperation.find(query,
				Discount.class, "discounts");
		Discounts discounts = new Discounts();
		discounts.getDiscount().addAll(discountsFound);
		
		return discounts;
	}

	
}
