package com.wupiti.dao;

import com.wupiti.domain.search.SearchDiscounts;

public interface SearchDao {

	public SearchDiscounts findNear(Double latitud, Double longitud,
			Double distance);

	public SearchDiscounts findNear(Double lat1, Double lon1, Double lat2, Double lon2);
	
	public SearchDiscounts findNear(Double latitud, Double longitud, Integer category);
	
	public SearchDiscounts find(String text);
	
	public SearchDiscounts getAll();
	
	public SearchDiscounts getDiscountsByCategory(Integer category);
	
	/* ---------------- NEW SERVICE v2 --------------- */
	
	public SearchDiscounts getDiscountsByCategoryv2(Integer category);
	
	public SearchDiscounts getAllv2();
	
	public SearchDiscounts findNearv2(Double lat1, Double lon1, Double lat2, Double lon2);
	
}
