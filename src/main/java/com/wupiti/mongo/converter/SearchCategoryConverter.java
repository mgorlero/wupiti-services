package com.wupiti.mongo.converter;

import java.util.ArrayList;
import java.util.List;

import org.springframework.core.convert.converter.Converter;

import com.mongodb.DBObject;
import com.wupiti.domain.search.SearchCategories;
import com.wupiti.domain.search.SearchCategory;

public class SearchCategoryConverter  implements Converter<DBObject, SearchCategories>{

	public SearchCategories convert(DBObject source) {
		SearchCategories sbrs = new SearchCategories();
		List<SearchCategory> sbs = new ArrayList<SearchCategory>();
		for (String s : source.keySet()) {
			DBObject dbobj = (DBObject)source.get(s);
			SearchCategory sb = new SearchCategory();
			sb.setName((String)dbobj.get("name"));
			sb.setCategoryId((Integer)dbobj.get("categoryId"));
			sb.setParent((Integer)dbobj.get("parent"));
			sbs.add(sb);
		}
		sbrs.setCategory(sbs);
	    return sbrs;
	  }
	
}

