package com.wupiti.mongo.converter;

import org.springframework.core.convert.converter.Converter;

import com.mongodb.DBObject;
import com.wupiti.domain.discount.DiscountImages;

public class DiscountImageConverter implements Converter<DBObject, DiscountImages> {

	public DiscountImages convert(DBObject source) {
		DiscountImages images = new DiscountImages();
		String[] image = new String[source.keySet().size()];
		int i = 0;
		for (String s : source.keySet()) {
			image[i] = (String) source.get(s);
			i++;
		}
		images.setImage(image);
		return images;
	}
}
