package com.wupiti.mongo.converter;

import java.util.ArrayList;
import java.util.List;

import org.springframework.core.convert.converter.Converter;

import com.mongodb.DBObject;
import com.wupiti.domain.search.SearchBranch;
import com.wupiti.domain.search.SearchBranches;

public class SearchBranchConverter  implements Converter<DBObject, SearchBranches>{

	public SearchBranches convert(DBObject source) {
		SearchBranches sbrs = new SearchBranches();
		List<SearchBranch> sbs = new ArrayList<SearchBranch>();
		for (String s : source.keySet()) {
			DBObject dbobj = (DBObject)source.get(s);
			SearchBranch sb = new SearchBranch();
			sb.setBranchId((Integer)dbobj.get("branchId"));
			sb.setLat((Double)dbobj.get("lat"));
			sb.setLon((Double)dbobj.get("lon"));
			sb.setDetails(dbobj.get("details").toString());
			sb.setDescription(dbobj.get("description").toString());
			sb.setFacebookPlace(dbobj.get("facebookPlace").toString());
			sbs.add(sb);
		}
		sbrs.setBranch(sbs);
	    return sbrs;
	  }
	
}

