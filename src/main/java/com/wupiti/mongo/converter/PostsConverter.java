package com.wupiti.mongo.converter;

import java.util.ArrayList;
import java.util.List;

import org.springframework.core.convert.converter.Converter;

import com.mongodb.DBObject;
import com.wupiti.domain.post.Post;
import com.wupiti.domain.post.Posts;


public class PostsConverter  implements Converter<DBObject, Posts>{

	public Posts convert(DBObject source) {
		Posts sbrs = new Posts();
		List<Post> sbs = new ArrayList<Post>();
		for (String s : source.keySet()) {
			DBObject dbobj = (DBObject)source.get(s);
			Post sb = new Post();
			
			sb.setBranchId((String)dbobj.get("branchId"));
			sb.setPlaceId((Integer)dbobj.get("placeId"));
			sb.setPostNetwork((String)dbobj.get("postNetwork"));
			sb.setUserId((String)dbobj.get("userId"));
			sb.setImage((String)dbobj.get("image"));
			sb.setIsStockImagePost((Boolean)dbobj.get("stockImagePost"));
			sb.setCuponBarcode((String)dbobj.get("cuponBarcode"));
			sb.setPostMessage((String)dbobj.get("postMessage"));
			
			sbs.add(sb);
		}
		//sbrs.setPost(sbs);
	    return sbrs;
	  }
	
}

