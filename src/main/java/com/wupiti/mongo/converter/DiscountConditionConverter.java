package com.wupiti.mongo.converter;

import org.springframework.core.convert.converter.Converter;

import com.mongodb.DBObject;
import com.wupiti.domain.discount.Conditions;

public class DiscountConditionConverter implements
		Converter<DBObject, Conditions> {

	public Conditions convert(DBObject source) {
		Conditions conds = new Conditions();
		String[] cond = new String[source.keySet().size()];
		int i = 0;
		for (String s : source.keySet()) {
			cond[i] = (String) source.get(s);
			i++;
		}
		conds.setCondition(cond);
		return conds;
	}
}
